import React, {useEffect, useState} from 'react'
import { View, Text, StyleSheet, ScrollView, SafeAreaView } from "react-native";
import api from '../config/api';
import OrderCard from '../components/orderCard'
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

export default function Main({navigation}) {
    const user = navigation.getParam('user')
    console.log(user);
    

    const [orders, setOrders] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const ordersData = await api.get(`/users/${user.id}/orders`)
            setOrders(ordersData.data.orders);
        }
        fetchData()
    },[])

    const createOrderIfNotExists = async () => {
        const order = await AsyncStorage.getItem('order')
        console.log('a', order);
        
        if(order){
            return order
        }
        const data = {
            user_id: user.id,
            code: generateRandomCode(8),
            status: 'pendente',
            total: 0
        }
        const result = await api.post('/orders', data)
        await AsyncStorage.setItem('order', result.data)
        console.log('r', result.data);
        
        return result.data
    }

    const generateRandomCode = (() => {
        const USABLE_CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789".split("");
      
        return length => {
          return new Array(length).fill(null).map(() => {
            return USABLE_CHARACTERS[Math.floor(Math.random() * USABLE_CHARACTERS.length)];
          }).join("");
        }
      })();
    
    const handlerMakeOrder = async () => {
        const order = await createOrderIfNotExists()
        
        navigation.navigate('Products', {user, order})
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <TouchableOpacity onPress={handlerMakeOrder} style={styles.button}>
                <Text style={styles.buttonText}>Fazer pedido</Text>
            </TouchableOpacity>
            <ScrollView style={{flex: 1, alignSelf: 'stretch'}}>
                <View style={styles.cardsContainer}>
                    {
                        orders.map((order, key) => (
                            <OrderCard order={order} key={key}/>
                        ))
                    }
                </View>
            </ScrollView>
        </SafeAreaView>
    )

}

const styles = StyleSheet.create({
    container: {
        alignSelf: 'stretch',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#fcf7e5',
        padding: 30,
    },

    cardsContainer: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
    },
    
    button: {
        height: 46,
        alignSelf: 'stretch',
        backgroundColor: '#199c7b',
        borderRadius: 5,
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        width: 150
      },
    
      buttonText: {
          color: '#FFF',
          fontWeight: 'bold',
          fontSize: 16
      }
})