import React, {useState, useEffect} from 'react';
import { Text, StyleSheet, View, TextInput, Image, TouchableOpacity } from 'react-native';
import logo from '../assets/logo.png'
import api from '../config/api'
import AsyncStorage from '@react-native-community/async-storage'

export default function Login({navigation}) {

  const [user, setUser] = useState({
      email: 'rodrigoklaes@gmail.com',
      pass: '123'
  })

  // useEffect(() => {
  //   AsyncStorage.getItem('user').then(user => {
  //     if(user){        
  //       navigation.navigate('Main', {user: JSON.parse(user)})
  //     }
  //   })
  // }, [])
  
  
  const handleLogin = async () => {
    
      const result = await api.post('/auth/login', user)
      await AsyncStorage.setItem('user', JSON.stringify(result.data))
      navigation.navigate('Main', {user: result.data});
      
  }
  
  return (
    <View style={styles.container}>
        <Image source={logo} style={styles.logo}/>
        <TextInput 
            autoCapitalize='none' 
            placeholder='Digite seu email...' 
            style={styles.input}
            value={user.email}
            onChangeText={(text) => setUser({...user, email: text})}
        />
        <TextInput
            secureTextEntry={true} 
            autoCapitalize='none' 
            placeholder='Digite sua senha...' 
            style={styles.input}
            value={user.pass}
            onChangeText={(text) => setUser({...user, pass: text})}
        />
        <TouchableOpacity onPress={handleLogin} style={styles.button}>
            <Text style={styles.buttonText}>
                Entrar
            </Text>
        </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fcf7e5',
    padding: 30,
    alignSelf: 'stretch'
  },

  logo:{
      width: 130,
      height: 54
  },

  input: {
    height: 46,
    alignSelf: 'stretch',
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#199c7b',
    borderRadius: 5,
    marginTop: 20,
    paddingHorizontal: 15
  },

  button: {
    height: 46,
    alignSelf: 'stretch',
    backgroundColor: '#199c7b',
    borderRadius: 5,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonText: {
      color: '#FFF',
      fontWeight: 'bold',
      fontSize: 16
  }
})
