import { createAppContainer, createSwitchNavigator } from "react-navigation";

import Login from '../pages/login';
import Main from '../pages/main';
import Products from '../pages/products';

export default createAppContainer(
    createSwitchNavigator({
        Login,
        Products,
        Main,
    })
)