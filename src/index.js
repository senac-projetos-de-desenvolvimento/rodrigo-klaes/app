import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import Routes from './routes'

export default function App() {
  return (
    <Routes />
  );
};